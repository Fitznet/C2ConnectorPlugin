﻿
using Concept2;
using EasyErgsocket;
using FitzLanePlugin.Interfaces;

using System.IO;
using System.Runtime.Serialization.Json;

namespace C2Connector
{
    class C2Connector : IPlayer
    {
        EasyErgsocket.Erg thisErg;
        PerformanceMonitor pm;

        public C2Connector(string name, string config)
        {
            //read the config
            MemoryStream memStream = new MemoryStream();
            StreamWriter strWriter = new StreamWriter(memStream);
            strWriter.Write(config);
            strWriter.Flush();
            memStream.Position = 0;
            DataContractJsonSerializer playerSerializer = new DataContractJsonSerializer(typeof(C2ConnectorConfig));
            C2ConnectorConfig cfg = (C2ConnectorConfig)playerSerializer.ReadObject(memStream);

            //init the C2 interface
            PMUSBInterface.Initialize();
            PMUSBInterface.InitializeProtocol(999);

            ushort numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3TESTER_PRODUCT_NAME);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3_PRODUCT_NAME);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3_PRODUCT_NAME2);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM4_PRODUCT_NAME);
            numErgs += PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM5_PRODUCT_NAME);
            if(numErgs == 0)
            {
                //TODO: This means that no erg has been found... what to do?
            }
            
            pm = new PerformanceMonitor((ushort)cfg.ErgAddress);
            //init everything, this avoids us crashing if no connection is made to the erg
            pm.Calories = 0;
            pm.DeviceNumber = 0;
            pm.Distance = 0.0f;
            pm.DragFactor = 0;
            pm.Heartrate = 0;
            pm.Pace = 0.0f;
            pm.Power = 0;
            pm.Serial = "";
            pm.SPM = 0;
            pm.SPMAvg = 0.0f;
            pm.StrokePhase = StrokePhase.Idle;
            pm.Worktime = 0.0f;

            try
            {
                pm.StatusUpdate();
            }
            catch (PMUSBInterface.PMUSBException ex)
            {
                //No Erg found, USB exception...
            }


            thisErg = new EasyErgsocket.Erg();
            thisErg.cadence = 0;
            thisErg.calories = 0;
            thisErg.distance = 0.0;
            thisErg.ergtype = EasyErgsocket.ErgType.ROW;
            thisErg.exerciseTime = 0.0;
            thisErg.heartrate = 0;
            thisErg.name = name;
            thisErg.paceInSecs = 0;
            thisErg.playertype = EasyErgsocket.PlayerType.HUMAN;
            thisErg.power = 0;
            thisErg.ergId = pm.Serial;
        }

        public string ParentId
        {
            get; set;
        }

        public Erg GetErg()
        {
            return thisErg;
        }

        public void Reset()
        {}

        public void Update(Erg givenParent = null)
        {
            //NOTE: This erg ignores parent ergometers...
            try
            {
                pm.LowResolutionUpdate();
            }
            catch (PMUSBInterface.PMUSBException ex)
            {
                //No Erg found, USB exception...
            }

            thisErg.cadence = pm.SPM;
            thisErg.calories = pm.Calories;
            thisErg.distance = pm.Distance;
            thisErg.exerciseTime = pm.Worktime;
            thisErg.heartrate = pm.Heartrate;
            thisErg.paceInSecs = (uint)pm.Pace;
            thisErg.power = pm.Power;
        }
    }
}
